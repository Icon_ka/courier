package models

import (
    cm "gitlab.com/Icon_ka/courier/module/courier/models"
    om "gitlab.com/Icon_ka/courier/module/order/models"
)

type CourierStatus struct {
    Courier cm.Courier `json:"courier"`
    Orders  []om.Order `json:"orders"`
}
