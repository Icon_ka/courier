package storage

import (
    "context"
    "encoding/json"
    "github.com/go-redis/redis/v8"
    "gitlab.com/Icon_ka/courier/module/courier/models"
    "log"
)

type CourierStorager interface {
    Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
    GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
    storage *redis.Client
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
    obj, err := json.Marshal(courier)
    if err != nil {
        return err
    }
    err = c.storage.Set(ctx, "courier", obj, 0).Err()
    if err != nil {
        return err
    }

    return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
    courier := models.Courier{}
    data, err := c.storage.Get(ctx, "courier").Bytes()
    if err != nil {
        if err == redis.Nil {
            log.Println("RETURNING nil nil")
            return nil, nil
        }
        return nil, err
    }

    err = json.Unmarshal(data, &courier)
    if err != nil {
        log.Println("Problems while unmarshal")
        return nil, err
    }

    return &courier, nil
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
    return &CourierStorage{storage: storage}
}
