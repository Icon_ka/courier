package service

import (
    "context"
    "gitlab.com/Icon_ka/courier/geo"
    "gitlab.com/Icon_ka/courier/module/courier/models"
    "gitlab.com/Icon_ka/courier/module/courier/storage"
    "log"
    "math"
)

// Направления движения курьера
const (
    DirectionUp    = 0
    DirectionDown  = 1
    DirectionLeft  = 2
    DirectionRight = 3
)

const (
    DefaultCourierLat = 59.9311
    DefaultCourierLng = 30.3609
)

type Courierer interface {
    GetCourier(ctx context.Context) (*models.Courier, error)
    MoveCourier(courier models.Courier, direction, zoom int) error
}

type CourierService struct {
    courierStorage storage.CourierStorager
    allowedZone    geo.PolygonChecker
    disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
    return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
    // получаем курьера из хранилища используя метод GetOne из storage/courier.go
    courier, err := c.courierStorage.GetOne(ctx)

    if err != nil {
        log.Println("ERR ON GET ONE FUNC", err)
        return nil, err
    }

    if courier == nil {
        courier = &models.Courier{
            Score: 0,
            Location: models.Point{
                Lat: DefaultCourierLat,
                Lng: DefaultCourierLng,
            },
        }
    }

    // проверяем, что курьер находится в разрешенной зоне
    // если нет, то перемещаем его в случайную точку в разрешенной зоне
    // сохраняем новые координаты курьера
    if !geo.CheckPointIsAllowed(geo.Point(courier.Location), c.allowedZone, c.disabledZones) {
        point := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
        courier.Location.Lat = point.Lat
        courier.Location.Lng = point.Lng

    }

    err = c.courierStorage.Save(ctx, *courier)
    if err != nil {
        return nil, err
    }
    return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
    // точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
    // 14 - это максимальный зум карты
    formula := 0.001 / (math.Pow(2.0, float64(zoom-14)))
    // далее нужно проверить, что курьер не вышел за границы зоны
    // если вышел, то нужно переместить его в случайную точку внутри зоны
    switch direction {
    case 0:
        courier.Location.Lng += formula
    case 1:
        courier.Location.Lng -= formula
    case 2:
        courier.Location.Lat -= formula
    case 3:
        courier.Location.Lat += formula
    }
    if !geo.CheckPointIsAllowed(geo.Point(courier.Location), c.allowedZone, c.disabledZones) {
        point := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
        courier.Location.Lat = point.Lat
        courier.Location.Lng = point.Lng
    }
    // далее сохранить изменения в хранилище
    err := c.courierStorage.Save(context.Background(), courier)

    return err
}
