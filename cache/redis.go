package cache

import (
	"fmt"
	"github.com/go-redis/redis/v8"
)

func NewRedisClient(host, port string) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", host, port),
	})
	return client
}
