package geo

import (
    "bufio"
    "github.com/brianvoe/gofakeit"
    geo "github.com/kellydunn/golang-geo"
    "os"
    "path/filepath"
    "strconv"
    "strings"
)

type Point struct {
    Lat float64 `json:"lat"`
    Lng float64 `json:"lng"`
}

type PolygonChecker interface {
    Contains(point Point) bool // проверить, находится ли точка внутри полигона
    Allowed() bool             // разрешено ли входить в полигон
    RandomPoint() Point        // сгенерировать случайную точку внутри полигона
}

type Polygon struct {
    polygon *geo.Polygon
    allowed bool
}

func (p *Polygon) Contains(point Point) bool {
    return p.polygon.Contains(geo.NewPoint(point.Lat, point.Lng))
}

func (p *Polygon) Allowed() bool {
    return p.allowed
}
func min(a, b float64) float64 {
    if a > b {
        return b
    }
    return a
}
func max(a, b float64) float64 {
    if a > b {
        return a
    }
    return b
}
func (p *Polygon) RandomPoint() Point {

    points := p.polygon.Points()
    i := gofakeit.Number(0, len(points)-1)
    j := gofakeit.Number(0, len(points)-1)
    lng := gofakeit.Float64Range(min(points[i].Lng(), points[j].Lng()), max(points[i].Lng(), points[j].Lng()))
    lat := gofakeit.Float64Range(min(points[i].Lat(), points[j].Lat()), max(points[i].Lat(), points[j].Lat()))

    return Point{
        Lat: lat,
        Lng: lng,
    }
}

func NewPolygon(points []Point, allowed bool) *Polygon {
    // используем библиотеку golang-geo для создания полигона
    geoPoints := make([]*geo.Point, len(points))
    for i, point := range points {
        geoPoints[i] = geo.NewPoint(point.Lat, point.Lng)
    }

    return &Polygon{
        polygon: geo.NewPolygon(geoPoints),
        allowed: allowed,
    }
}

func CheckPointIsAllowed(point Point, allowedZone PolygonChecker, disabledZones []PolygonChecker) bool {
    // проверить, находится ли точка в разрешенной зоне
    for _, elem := range disabledZones {
        if elem.Contains(point) {
            return false
        }
    }
    if !allowedZone.Contains(point) {
        return false
    }

    return true
}
func checkNotAllowedContain(point Point, dizabledZones []PolygonChecker) bool {
    for _, elem := range dizabledZones {
        if elem.Contains(point) {
            return true
        }
    }
    return false
}

func GetRandomAllowedLocation(allowedZone PolygonChecker, disabledZones []PolygonChecker) Point {
    var point Point
    point = allowedZone.RandomPoint()
    for {
        point = allowedZone.RandomPoint()
        if CheckPointIsAllowed(point, allowedZone, disabledZones) {
            break
        }
    }
    return point
}

func NewDisAllowedZone1() *Polygon {
    // добавить полигон с разрешенной зоной
    path, err := os.Getwd()
    if err != nil {
        panic(err)
    }

    if strings.Contains(path, "service") {
        path = "../../../public/js/polygon.js"
    } else {
        path = filepath.Join(path, "public/js/polygon.js")
    }

    file, err := os.Open(path)
    defer file.Close()
    if err != nil {
        panic(err)
    }

    points := []Point{}

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := scanner.Text()
        if strings.Contains(line, "noOrdersPolygon1") {
            for scanner.Scan() {
                line := scanner.Text()
                if strings.Contains(line, "];") {
                    return NewPolygon(points, false)
                }
                line = strings.TrimSpace(line)
                line = strings.Replace(line, "[", "", 1)
                line = strings.Replace(line, "]", "", 1)
                line = strings.Replace(line, ",", "", 2)
                coords := strings.Split(line, " ")
                coord1, _ := strconv.ParseFloat(coords[0], 64)
                coord2, _ := strconv.ParseFloat(coords[1], 64)
                points = append(points, Point{
                    Lat: coord1,
                    Lng: coord2,
                })
            }
            return NewPolygon(points, false)
        }
    }
    return NewPolygon(points, false)
}

func NewDisAllowedZone2() *Polygon {
    path, err := os.Getwd()
    if err != nil {
        panic(err)
    }

    if strings.Contains(path, "service") {
        path = "../../../public/js/polygon.js"
    } else {
        path = filepath.Join(path, "public/js/polygon.js")
    }

    file, err := os.Open(path)
    defer file.Close()
    if err != nil {
        panic(err)
    }

    points := []Point{}

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := scanner.Text()
        if strings.Contains(line, "noOrdersPolygon2") {
            for scanner.Scan() {
                line := scanner.Text()
                if strings.Contains(line, "];") {
                    return NewPolygon(points, false)
                }
                line = strings.TrimSpace(line)
                line = strings.Replace(line, "[", "", 1)
                line = strings.Replace(line, "]", "", 1)
                line = strings.Replace(line, ",", "", 2)
                coords := strings.Split(line, " ")
                coord1, _ := strconv.ParseFloat(coords[0], 64)
                coord2, _ := strconv.ParseFloat(coords[1], 64)
                points = append(points, Point{
                    Lat: coord1,
                    Lng: coord2,
                })
            }
            return NewPolygon(points, false)
        }
    }

    return NewPolygon(points, false)
}

func NewAllowedZone() *Polygon {
    // добавить полигон с разрешенной зоной
    // полигоны лежат в /public/js/polygons.js
    path, err := os.Getwd()
    if err != nil {
        panic(err)
    }
    if strings.Contains(path, "service") {
        path = "../../../public/js/polygon.js"
    } else {
        path = filepath.Join(path, "public/js/polygon.js")
    }

    file, err := os.Open(path)
    defer file.Close()
    if err != nil {
        panic(err)
    }

    points := []Point{}

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := scanner.Text()
        if strings.Contains(line, "mainPolygon") {
            for scanner.Scan() {
                line := scanner.Text()
                if strings.Contains(line, "];") {
                    return NewPolygon(points, true)
                }
                line = strings.TrimSpace(line)
                line = strings.Replace(line, "[", "", 1)
                line = strings.Replace(line, "]", "", 1)
                line = strings.Replace(line, ",", "", 2)
                coords := strings.Split(line, " ")
                coord1, _ := strconv.ParseFloat(coords[0], 64)
                coord2, _ := strconv.ParseFloat(coords[1], 64)
                points = append(points, Point{
                    Lat: coord1,
                    Lng: coord2,
                })
            }
            return NewPolygon(points, true)
        }
    }
    return NewPolygon(points, true)
}
