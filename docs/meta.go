// Package classification go-courier
//
// Documentation courier-service on go.
//
//	Schemes:
//	- http
//	- https
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
// swagger:meta
package docs

//go:generate swagger generate spec -o ../public/swagger.json --scan-models
